from machine import Pin, Timer
import time
import json
import sys
import uselect

time.sleep_ms(1000) # Give micropython time to boot up

BLINK_LED = True # Whether or not to blink the onboard LED when stepping the motor

def step_motor(pin, led_pin):
    if BLINK_LED:
        led_pin.on()
    pin.on()
    time.sleep_us(5)
    pin.off()
    led_pin.off()

def count_bits(n):
    bin(n).count("1")

class StepperMotor():
    FULL_STEP_COUNTS_PER_REV = 200
    SWITCH_CHECK_COUNTS = 400

    def __init__(self, step_pin, dir_pin, en_pin):
        self.step_pin = Pin(step_pin, Pin.OUT)
        self.dir_pin = Pin(dir_pin, Pin.OUT)
        self.en_pin = Pin(en_pin, Pin.OUT)
        self.en_pin.value(0)
        self.fast_update_thresh = self.SWITCH_CHECK_COUNTS/20

    def step_motor(self, direction=1):
        self.en_pin.value(1)
        self.dir_pin.value(direction<1)
        self.step_pin.on()
        time.sleep_us(1000)
        self.step_pin.off()
        self.en_pin.value(0)
    
    def multi_step_motor(self, steps, step_wait, switch=None):
        """
        :param: steps number of steps to take
        :param: step_wait number of microseconds per step
        """
        switch_tripped = False
        if not steps:
            return True
        if switch:
            switch.reset_state()
        self.en_pin.value(1)
        self.dir_pin.value(steps<0)
        time.sleep_us(1000)
        steps = abs(steps)
        step_wait = int(step_wait/2)
        for i in range(0,steps):
            self.step_pin.on()
            time.sleep_us(step_wait)
            if i%self.SWITCH_CHECK_COUNTS == 0:
                if switch:
                    if switch.tripped():
                        print("Hit switch, stopping")
                        switch_tripped = True
                        break
            elif i%self.fast_update_thresh == 0:
                if switch:
                    switch.fast_update()
            self.step_pin.off()
            time.sleep_us(step_wait)
        self.en_pin.value(0)
        return not switch_tripped

class MotionStage():
    MOVE_BACKWARD = 0
    MOVE_FORWARD = 1
    LOW_SOFTSTOP = 5 # mm
    HIGH_SOFTSTOP = 295 # mm
    SLEW_SPEED = 25 #mm/s
    HOMING_SPEED = 10 # mm/s
    HOMING_DISTANCE = 300 #ms

    def __init__(self, threads_per_mm, steps_per_rev, neg_switch, pos_switch, motor):
        self.motor = motor
        self.pos_switch = LimitSwitch(pos_switch)
        self.neg_switch = LimitSwitch(neg_switch)
        self.steps_per_rev = steps_per_rev
        self.threads_per_mm = threads_per_mm
        self.full_step_res = steps_per_rev/self.motor.FULL_STEP_COUNTS_PER_REV
        print("Step resolution: {}".format(self.full_step_res))
        self.position = 0
        self.use_softstops = True
        self.homed = False
        self.low_softstop = MotionStage.LOW_SOFTSTOP
        self.high_softstop = MotionStage.HIGH_SOFTSTOP

    def home_stage(self, skip_if_ok=False):
        if skip_if_ok and self.homed:
            return
        self.neg_switch.reset_state()
        print('Homing stage')
        self.move_abs(-self.HOMING_DISTANCE, self.HOMING_SPEED, monitor_switch=True, is_homing_move=True)
        self.position = 0
        print('Stage homed')
        self.neg_switch.reset_state()
        if self.neg_switch.tripped():
            self.homed = True
            self.move_abs(self.LOW_SOFTSTOP, self.SLEW_SPEED, monitor_switch=False)
        else:
            print("Failed to find negative limit switch, stage not homed!")
    
    def distance2steps(self, distance): # distance in mm
        steps = self.steps_per_rev*(distance*self.threads_per_mm)
        return int(float(self.full_step_res) * round(float(steps)/float(self.full_step_res)))
    
    def steps2distance(self, steps):
        return (float(steps)/float(self.steps_per_rev))/self.threads_per_mm

    def enforce_softstops(self, position):
        return min(max(self.low_softstop, position), self.high_softstop) if self.use_softstops else position

    def move_abs(self, position, speed, monitor_switch=True, is_homing_move=False): # Position is in mm, speed in mm/s
        if not self.homed and not is_homing_move:
            print("Rejecting motion, stage isn't homed")
            return
        if not is_homing_move:
            position = self.enforce_softstops(position)
        distance = position - self.position
        time = int((float(distance)/float(speed))*1000000)
        counts = self.distance2steps(distance)
        if counts == 0: # Don't divide by zero
            us_per_step = 1
        else:
            us_per_step = time/counts
        position = self.position + self.steps2distance(counts)
        print("Moving {} counts to position {}mm".format(counts, position))
        if monitor_switch:
            switch = self.neg_switch if counts < 0 else self.pos_switch
        else:
            switch = None
        status = self.motor.multi_step_motor(counts, us_per_step, switch=switch)
        if not status and not is_homing_move: # we hit a switch
            self.homed = False
            print("Stage state unknown, too close to hardstop")
        self.position = position
    
    def move_rel(self, offset, speed): # Offset in mm, speed in mm/s
        position = self.enforce_softstops(self.position + offset)
        self.move_abs(position, speed)

    def set_softstops(self, low, high, enable):
        self.use_softstops = True if enable else False
        self.low_softstop = low
        self.high_softstop = high

    def show(self):
        self.pos_switch.reset_state()
        self.neg_switch.reset_state()
        print("===Motion Stage State===")
        print("Homing State: {} HOMED".format("" if self.homed else "NOT"))
        print("Pos. Switch: {}, Neg. Switch: {}".format(self.pos_switch.tripped(), self.neg_switch.tripped()))
        print("Position: {:.3f} mm".format(self.position))
        print("Softstops: [{}, {}] mm, Enabled: {}".format(self.low_softstop, self.high_softstop, self.use_softstops))
        print("Steps per rev: {}".format(self.steps_per_rev))
        print("Step resolution: {}".format(self.full_step_res))
        
class LimitSwitch():
    def __init__(self, pin):
        self.pin = Pin(pin, Pin.IN, Pin.PULL_UP)
        self.buff = 0

    def tripped(self):
        return not self.pin.value()
    
    def reset_state(self):
        for i in range(20):
            self.pin.value()
    
    def value(self):
        self.buff<<=1
        self.buff |= self.pin.value()
        self.buff &= 0xfffff # 20 bits
        n = bin(self.buff).count("1") # Count the number of 1s in buff
        return n>10
    
    def fast_update(self):
        self.buff<<=1
        self.buff = (self.buff | self.pin.value()) & 0xfffff # 20 bits


class ZStackCamera():
    def __init__(self, stage, shutter_pin):
        """
        :param: loop_interval in minutes
        :param: settling_wait whole number seconds
        :param: exposure_wait whole number seconds
        :param: num_loops Number of observations to do, use -1 (or any n<0) to loop forever
        :param: obs_speed speed to do the mid-observation moves in mm/s
        """
        self.stage = stage
        self.observation_active = False
        self.shutter_pin = Pin(shutter_pin, Pin.OUT)
        self.shutter_pin.low()
        self.configured = False
        self.armed = False

    def config(self, start, end, num_stops, loop_interval, settling_wait, exposure_wait, num_loops, obs_speed):
        self.start_pos = start
        self.spacing = (end-start)/(num_stops-1)
        self.end_pos = end
        self.num_stops = num_stops
        self.settling_wait = settling_wait
        self.exposure_wait = exposure_wait
        self.num_loops = num_loops
        self.loop_counter = 0
        self.image_counter = 0
        self.loop_interval = loop_interval
        self.obs_speed = obs_speed
        self.configured = True
        self.armed = False
        obs_time = self.compute_obs_time()
        if ((self.loop_interval*60) - obs_time) <= ((loop_interval*60)*0.2):
            print("Warning, less than 20% margin to overlapping observations. Loop interval {:.3f} s, approx obs time {:.3f} s".format(self.loop_interval*60, obs_time))

    def load_from_file(self, file):
        try:
            with open(file, 'r') as f:
                data = json.load(f)
        except OSError:
            self.configured = False
            print("Failed to open {}".format(file))
            return
        if any(s not in data for s in ['start', 'end', 'num_stops', 'loop_interval', 'settling_wait', 'exposure_wait', 'num_loops', 'obs_speed']):
            self.configured = False
            print("Configuration file {} is missing expected configuration options".format(file))
            return
        self.config(
            float(data['start']),
            float(data['end']),
            int(data['num_stops']),
            float(data['loop_interval']),
            float(data['settling_wait']),
            float(data['exposure_wait']),
            int(data['num_loops']),
            float(data['obs_speed'])
        )
        print("Successfully loaded {}".format(file))

    def compute_obs_time(self):
        if self.configured:
            frame_time = self.settling_wait + self.exposure_wait + (self.spacing/self.obs_speed)
            return frame_time * self.num_stops
        else:
            return 0

    def start(self):
        if self.armed:
            print("Observation is already armed! Stop it to reconfigure.")
        elif self.configured:
            self.stage.home_stage(skip_if_ok=True)
            self.stage.move_abs(self.start_pos, self.stage.SLEW_SPEED)
            period = self.loop_interval*60*1000
            self.armed = True
            self.show()
            self.timer = Timer(period=int(period), mode=Timer.PERIODIC, callback=lambda t: self.do_observation())
            self.do_observation()
        else:
            print("Can't start camera, observation is not configured")

    def slew_to_start(self):
        if self.configured:
            self.stage.move_abs(self.start_pos, self.stage.SLEW_SPEED)

    def stop(self):
        if self.armed:
            self.timer.deinit()
        print("Stopping looped imaging")
        self.armed = False

    def do_observation(self):
        if self.observation_active:
            print("Overlapping observations, skipping this one...")
            return
        self.observation_active = True
        self.stage.move_abs(self.start_pos, self.obs_speed)
        for i in range(self.num_stops):
            time.sleep_ms(int(self.settling_wait*1000))
            self.shutter()
            time.sleep_ms(int(self.exposure_wait*1000))
            if i < self.num_stops-1:
                self.stage.move_rel(self.spacing, self.obs_speed)
        self.stage.move_abs(self.start_pos, self.stage.SLEW_SPEED)
        self.loop_counter += 1
        if self.loop_counter >= self.num_loops and self.num_loops > 0:
            self.stop()
        self.observation_active = False
        
    def shutter(self):
        self.shutter_pin.high()
        print("Triggered shutter.")
        time.sleep_ms(100)
        self.shutter_pin.low()
        self.image_counter += 1

    def show(self):
        if not self.configured:
            print("No observation configuration is loaded")
        else:
            print("===Observation State===")
            print("Observation Armed: {}".format('YES' if self.armed else 'NO'))
            print("Loop Counter: {}, Image Counter: {}\nNum Stops: {}".format(self.loop_counter, self.image_counter, self.num_stops))
            print("Start Pos: {:.3f} mm, End Pos: {:.3f} mm\nImage Spacing: {:.3f} mm\nObs. Speed: {:.3f} mm/s".format(self.start_pos, self.end_pos, self.spacing, self.obs_speed))
            print("Loop Interval: {} min\nSettling Wait: {} s\nExposure Wait: {} s".format(self.loop_interval, self.settling_wait, self.exposure_wait))
            print("Loop Limit: {}".format("infinite" if self.num_loops<0 else self.num_loops))
            print("Approx Obs Time: {:.3f} s".format(self.compute_obs_time()))

class Console():
    def __init__(self):
        self.buff = ''
        self.spoll = uselect.poll()
        self.spoll.register(sys.stdin, uselect.POLLIN)
    
    def read(self):
        c = (sys.stdin.read(1) if self.spoll.poll(0) else "")
        if c:
            self.buff += c
            self.buff = self.buff.replace('\n', '\r') # Replace newline characters with carriage return
            if self.buff[0] == '\r': # ignore carriage returns that are at the beginning of the string
                self.buff = self.buff[1:]
            if '\r' in self.buff:
                s = self.buff.split('\r', 1)
                s = [c for c in s if c] # Remove empty strings
                if len(s) == 0:
                    return None
                print(' '*len(self.buff), end='\r')
                if len(s) <= 1:
                    self.buff = ''
                else:
                    self.buff = s[1]
                return s[0]
            #self.show_console()
        return None

    def show_console(self):
        print(self.buff, end='')

motor = StepperMotor(0, 1, 4)
stage = MotionStage(0.25, 1600, 3, 2, motor)

console = Console()
camera = ZStackCamera(stage, 5)

def start_motion():
    global timer_armed
    timer_armed=False
    camera.start()

def get_config_filename():
    try:
        with open('what_file_to_load', 'r') as f:
            filename = f.readline()
        return filename
    except OSError:
        print("Failed to open what_file_to_load")
        return ""

camera.load_from_file(get_config_filename())

print('Send "stop" to stop observation start. First observation will start in 30 seconds otherwise.')
start_timer = Timer(period=30000, mode=Timer.ONE_SHOT, callback=lambda t: start_motion())
timer_armed = True

help = {
    'move_abs': 'move_abs pos speed\nMove to absolute position. pos in mm, speed in mm/s',
    'move_rel': 'move_rel offset speed\nMove to a relative offset from the current position. pos in mm, speed in mm/s',
    'do_obs': 'do_obs\nExecute the observation that is currently loaded',
    'set_obs': 'set_obs start end num_stops loop_interval settling_wait exposure_wait num_loops obs_speed\nAll in mm and s, except loop_interval which is minutes',
    'start': 'start\nStart looped observations using the currently loaded observation config',
    'stop': 'stop\nStop any active observation loop, any active observation will finish',
    'home': 'home\nRe-home the motion stage, must be done before executing any other motion after bootup',
    'take_image': 'take_image\nTrigger the shutter',
    'show': 'show_obs type\nShow the current configuration. Type argument may be "obs" or "stage"',
    #'softstops': 'softstops low high enable\nSet the softstops, third argment 0 disables softstops, 1 enables',
    'help': 'help [cmd]\nNo arguments lists all commands, specify a command to get command-specific help.'
}

def execute_command(cmd):
    global timer_armed
    cmd = [s.strip() for s in cmd.strip().split(' ')]
    #try:
    if cmd[0] == 'move_abs' and len(cmd) == 3:
        stage.move_abs(float(cmd[1]), float(cmd[2]))
    elif cmd[0] == 'move_rel' and len(cmd) == 3:
        stage.move_rel(float(cmd[1]), float(cmd[2]))
    elif cmd[0] == 'do_obs' and len(cmd) == 1:
        camera.slew_to_start()
        camera.do_observation()
    elif cmd[0] == 'set_obs' and len(cmd) == 9:
        # start, end, num_stops, loop_interval, settling_wait, exposure_wait, num_loops, obs_speed
        camera.config(float(cmd[1]), float(cmd[2]), int(cmd[3]), float(cmd[4]), float(cmd[5]), float(cmd[6]), int(cmd[7]), float(cmd[8]))
    elif cmd[0] == 'start' and len(cmd) == 1:
        start_timer.deinit()
        timer_armed=False
        camera.start()
    elif cmd[0] == 'stop':
        start_timer.deinit()
        timer_armed=False
        camera.stop()
    elif cmd[0] == 'home':
        stage.home_stage()
    elif cmd[0] == 'take_image' and len(cmd) == 1:
        camera.shutter()
    elif cmd[0] == 'help' and len(cmd) <= 2:
        if len(cmd) == 1:
            print("Available commands: {}".format(' '.join(list(help.keys()))))
        else:
            if cmd[1] in help:
                print(help[cmd[1]])
            else:
                print("{} is not a command".format(cmd[1]))
    elif cmd[0] == 'show' and len(cmd) == 2:
        if cmd[1] not in ['stage', 'obs']:
            print('{} is not a valid show option'.format(cmd[1]))
        elif cmd[1] == 'stage':
            stage.show()
        elif cmd[1] == 'obs':
            camera.show()
    #elif cmd[0] == 'softstops' and len(cmd) == 4:
    #    stage.set_softstops(float(cmd[1]), float(cmd[2]), int(cmd[3]))
    else:
        print('Command {} is invalid'.format(cmd))
    #except:
    #    print('Error parsing command {}'.format(cmd))

while True:
    cmd = console.read()
    if timer_armed:
        if stage.neg_switch.tripped() or stage.pos_switch.tripped():
            print("Disarming start timer from switch press")
            execute_command("stop")
    if cmd:
        execute_command(cmd)

