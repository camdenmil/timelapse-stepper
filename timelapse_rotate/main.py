from machine import Pin, Timer
import time
import json

time.sleep_ms(1000)

BLINK_LED = False # Whether or not to blink the onboard LED when stepping the motor

step_pin = Pin(0, Pin.OUT)
dir_pin = Pin(1, Pin.OUT)
led_pin = Pin("LED", Pin.OUT)

step_pin.off()
dir_pin.on()

def load_settings_from_file(filename):
    defaults = {
        "steps_per_rev": 3200, 
        "rotations_per_day": 8460,
        "blink_led_each_step": 1,
        "direction": 1
    }   
    try:
        with open(filename, 'r') as f:
            data = json.load(f)
    except OSError:
        data = defaults
    return data

def step_motor(pin, led_pin):
    if BLINK_LED:
        led_pin.on()
    pin.on()
    time.sleep_us(5)
    pin.off()
    led_pin.off()

def step_time(rotation_time, steps_per_rev):
    """
    Compute step period from rotation time and step resolution
    :param: rotation_time amount of time for one rotation in seconds
    :param: steps_per_revolution number of steps per revolution
    :return: Period of one step in milliseconds
    """
    return int((rotation_time*1000)/steps_per_rev)

settings = load_settings_from_file('/step_settings.json')

BLINK_LED = settings['blink_led_each_step']

step_time_ms = step_time(86400/float(settings['rotations_per_day']), settings['steps_per_rev'])
if step_time_ms < 2:
    step_time_ms = 2
if settings['direction']:
    dir_pin.on()
else:
    dir_pin.off()
print('Step time: {} millisecond{}'.format(step_time_ms, 's' if step_time_ms != 1 else ''))

tim = Timer(period=step_time_ms, mode=Timer.PERIODIC, callback=lambda t: step_motor(step_pin, led_pin))

while True:
    pass
